<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Log as LogModel ;

/**
* Контроллер логов
*/

class LogController extends Controller
{
	/**
	* Главная страница
	*
	* @return Response
	*/
	public function index( ) {
		return view( 'home' ) ;
	}

	/**
	* Страница логов
	* @param Request $request - объект зпроса
	* @return Response
	*/
	public function page( Request $request ) {
		$order_by = $request->input( 'order' ) ;
		$type = $request->input( 'type' ) ;
		$page = $request->input( 'page' ) ;

		return LogModel::get( $order_by , $type , $page ) ;
	}
}
