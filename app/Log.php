<?php

namespace App;

use Illuminate\Support\Facades\DB ;
use Illuminate\Database\Eloquent\Model ;

/**
* Модель логов
*/

class Log extends Model
{
	protected $table = 'log' ;
	public $timestamps = false ;

	/**
	* Список записей
	*
	* @param string $order_by - упорядочение по полю `ts`
	* @param integer $type - значения поля `type`
	* @param integer $page - номер страницы данных
	* @param integer $size - размер страницы данных
	* @return mixed - результат
	*/

	public static function get( $order_by = 'ASC' , $type = null , $page = 0 , $size = 100 ) {
		if ( ! in_array( $order_by , [ 'ASC' , 'DESC' ] ) ) {
			$order_by = 'ASC' ;
		}

		$args = [ ] ;
		$sql_where = [ ] ;
		$sql_from = '
FROM
	`log` AS `l1`
		' ;

		$count = $id_from = $id_to = 0 ;
		$order_field_name = 'id' ;

		if ( ! is_null( $type ) ) {
			$sql_where[] = '( `l1`.`type` = :type )' ;
			$args[ 'type' ] = $type ;

			$order_field_name = 'type_id' ;
		}

		$count = DB::select( "
SELECT
	max( `{$order_field_name}` ) AS `count`
$sql_from
		" . (
empty( $sql_where ) ? '' : '
WHERE
	( ' . implode( ' ) AND ( ' , $sql_where ) . ' )
		' ) , $args )[ 0 ]->count ;

		if ( $order_by == 'ASC' ) {
			$id_from = $page * $size ;
			$id_to = $id_from + $size ;
		} else {
			$id_to = $count - $page * $size ;
			$id_from = $id_to - $size ;

			if ( $id_from < 0 ) {
				$id_from = 0 ;
			}
		}

		$sql_where[] = '( `l1`.`' . $order_field_name . '` BETWEEN :id_from AND :id_to )' ;

		$args[ 'id_from' ] = $id_from ;
		$args[ 'id_to' ] = $id_to ;

		$sql = "
SELECT
	`l1`.`id` ,
	`l1`.`ts` ,
	`l1`.`type` ,
	`l1`.`message`
$sql_from
WHERE
	( " . implode( ' ) AND ( ' , $sql_where ) . " )
ORDER BY
	`l1`.`id` $order_by ;
		" ;

		return [
			'info' => [
				/*
				'sql' => $sql ,
				'args' => $args ,
				*/

				'pages' => ceil( $count / $size ) ,
				'order_by' => $order_by ,
				'page' => $page ,
				'type' => $type ,
			] ,
			'rows' => DB::select( $sql , $args ) ,
		] ;
	}
}
