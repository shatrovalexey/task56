<!DOCTYPE html>

<html>
	<head>
		<title>Task56</title>

		<meta charset="utf-8">

		<link
			rel="stylesheet"
			href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
			type="text/css"
		>
		<link rel="stylesheet" href="/css/style.css" type="text/css" >
		<script
			src="https://code.jquery.com/jquery-3.3.1.min.js"
			integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			crossorigin="anonymous"
		></script>
		<script src="/js/script.js"></script>
	</head>
	<body>
		<h1>Task56</h1>

		<form action="/page" method="get" class="special-form">
			<fieldset>
				<legend>
					<h2>Filter</h2>
				</legend>
				<label>
					<span>page</span>
					<input type="range" value="0" min="0" max="0" name="page" class="special-form-page" required>
				</label>
				<!--label>
					<span>switch type</span>
					<input type="checkbox" value="0" class="special-form-page-switch-type">
				</label-->
				<label>
					<span>type</span>
					<input type="range" value="1" min="1" max="10" name="type" class="special-form-type" required>
				</label>

				<span>order</span>
				<label>
					<span>ASC</span>
					<input type="radio" name="order" value="ASC" checked required>
				</label>
				<label>
					<span>DESC</span>
					<input type="radio" name="order" value="DESC" required>
				</label>
			</fieldset>
		</form>

		<h2 class="special-form-caption">
			<span>Log table</span>
			<span>
				page #<span data-field="page"></span> of
				<span data-field="pages"></span>
			</span>
			<span>
				type #<span data-field="type"></span>
			</span>
		</h2>
		<table class="special-form-table">
			<thead>
				<tr>
					<th>#</th>
					<th>ts</th>
					<th>type</th>
					<th>message</th>
				</tr>
			</thead>
			<tbody>
				<tr class="nod">
					<td data-field="id"></td>
					<td data-field="ts"></td>
					<td data-field="type"></td>
					<td data-field="message"></td>
				</tr>
			</tbody>
		</table>
	</body>
</html>