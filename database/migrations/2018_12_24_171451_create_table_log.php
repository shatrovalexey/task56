<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log', function (Blueprint $table) {
		$table->bigIncrements( 'id' ) ;
		$table->unsignedTinyInteger( 'type' ) ;
		$table->unsignedMediumInteger( 'type_id' ) ;
		$table->string( 'message' , 1e2 ) ;
		$table->dateTime( 'ts' ) ;

		$table->unique( [ 'type' , 'type_id' , 'id' ] ) ;
        });

	DB::statement( '
ALTER TABLE `log`
	MODIFY `type` TINYINT( 1 ) UNSIGNED NOT null DEFAULT 0 ,
	PACK_KEYS = 0 ,
	DELAY_KEY_WRITE = 1 ;
	' ) ;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log');
    }
}
