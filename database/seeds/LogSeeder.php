<?php
use Illuminate\Database\Seeder ;
use Illuminate\Database\Schema\Blueprint ;

class LogSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run( ) {
	$type_pos = array_fill( 0 , 11 , 0 ) ;
	$table = DB::table( 'log' ) ;
	$ts = DB::raw( 'now( )' ) ;

	for ( $i = 0 ; $i < 300e6 ; $i ++ ) {
		$type_current = rand( 1 , 10 ) ;

		$table->insert( [
			'ts' => $ts ,
			'type' => $type_current ,
			'type_id' => ++ $type_pos[ $type_current ] ,
			'message' => str_random( 0x50 ) ,
		] ) ;
	}

	DB::statement( '
OPTIMIZE TABLE `log` ;
	' ) ;
    }
}