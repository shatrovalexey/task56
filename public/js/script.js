jQuery( function( ) {
	jQuery( "input[type=radio]" ).on( "change" , function( ) {
		let $self = jQuery( this ) ;

		$self.attr( {
			"title" : $self.val( )
		} ) ;
	} ) ;

	let $caption = jQuery( ".special-form-caption" ) ;
	let $table = jQuery( ".special-form-table" ) ;
	let $tbody = $table.find( "tbody" ) ;
	let $tr = $tbody.find( "tr:first" ).clone( true ) ;

	/*
	jQuery( ".special-form-page-switch-type" ).on( "change" , function( ) {
		let $self = jQuery( this ) ;

		jQuery( ".special-form-type" ).prop( "disabled" , $self.prop( "checked" ) ) ;
	} ) ;
	*/

	jQuery( ".special-form" ).on( "submit" , function( ) {
		return false ;
	} ).on( "change" , function( ) {
		let $self = jQuery( this ) ;

		$tbody.empty( ) ;

		jQuery.ajax( {
			"url" : $self.attr( "action" ) ,
			"post" : $self.attr( "method" ) ,
			"data" : $self.serialize( ) ,
			"dataType" : "json" ,
			"success" : function( $data ) {
				$self.find( "input[name=type]" ).attr( {
					"value" : $data.info.type
				} ) ;
				$self.find( "input[name=page]" ).prop( "max" , $data.info.pages ).
				attr( {
					"value" : $data.info.page
				} ) ;
				$self.find( "input[name=order]" ).attr( {
					"value" : $data.info.order
				} ) ;

				jQuery( $data.rows ).each( function( $i , $item ) {
					let $trCurrent = $tr.clone( true ).removeClass( "nod" ) ;

					for ( let $key in $item ) {
						$trCurrent.find( "*[data-field=" + $key + "]" ).
							text( $item[ $key ] ) ;
					}

					$tbody.append( $trCurrent ) ;
				} ) ;

				$caption.find( "*[data-field=page]" ).text( $data.info.page ) ;
				$caption.find( "*[data-field=pages]" ).text( $data.info.pages ) ;
				$caption.find( "*[data-field=type]" ).text( $data.info.type ) ;
				$caption.find( "*[data-field=order_by]" ).text( $data.info.order_by ) ;
			}
		} ) ;

		return false ;
	} ).change( ) ;

	/*
	setInterval( function( ) {
		jQuery( ".special-form" ).change( ) ;
	} , 0x4000 ) ;
	*/
} ) ;